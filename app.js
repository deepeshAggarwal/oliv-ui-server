'use strict';

const express = require('express'),
  bodyParser = require('body-parser');
const app = express();


app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

const routes = require('./src/routes/index.js')(app);
const port = process.env.PORT || 3001;
app.listen(port, function() {
  console.log('listening on', port);
});

app.use(function(req, res, next) {
  res.setHeader('Access-Control-Allow-Origin', 'http://localhost:3000');
  res.setHeader('Access-Control-Allow-Methods', 'GET, OPTIONS');
  res.setHeader(
    'Access-Control-Allow-Headers',
    'Origin, X-Requested-With, Content-Type, Accept, Key, Access-Control-Allow-Origin, Authorization'
  );
  res.setHeader('Access-Control-Allow-Credentials', true);
  next();
});

module.exports = app;

