'use strict';

const request = require('superagent');
const urlParser = require('url');

var appRouter = function (app) {
  app.get("/search", function(req, res) {
  	res.setHeader('Access-Control-Allow-Origin', 'http://localhost:3000');
	  res.setHeader('Access-Control-Allow-Methods', 'GET, OPTIONS');
	  res.setHeader(
	    'Access-Control-Allow-Headers',
	    'Origin, X-Requested-With, Content-Type, Accept, Key, Access-Control-Allow-Origin, Authorization'
	  );
	  res.setHeader('Access-Control-Allow-Credentials', true);
  	let query = urlParser.parse(req.url).query;
    let url = 'https://api.yelp.com/v3/businesses/search?' + query;
    request
      .get(url)
      .set('Authorization', 'Bearer RfcYaddEVhfsFhyy9ZQWNM6LoSflk1mUydj6uZ3_ADJr6mapmI5vTHvz3aZxqz44vAX5jxKS8qY_woljd84J36J0FINhHlgOidD51NQPrwiLHWjNfGs8BySElIKVXHYx')
      .set('Accept', 'application/json')
      .then((response) => {
        res.send(response.body);
      }).catch((error) => {
      	res.status(500).send(error);
      });
  });
    

};
module.exports = appRouter;
